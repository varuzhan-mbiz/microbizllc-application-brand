import { NgModule }     from '@angular/core';
import { BrandModule  } from "./src/brand.module";

@NgModule( {
    imports: [ BrandModule ],
    exports: [ BrandModule ]
} )
export class MbizBrandModule {}

export * from "./src/brand.module";