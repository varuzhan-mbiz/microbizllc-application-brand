import { NgModule }            from '@angular/core';
import { FormsModule,
         ReactiveFormsModule } from '@angular/forms';
import { CommonModule }        from '@angular/common';
import { MaterialModule }      from '@angular/material';

import { GridModule }    from '@progress/kendo-angular-grid';
import { DialogModule }  from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

import { CoreModule } from '@microbizllc/application-core';

import { BrandComponent }     from './brand.component';
import { BrandRoutingModule } from './brand-routing.module';
import { BrandService }       from './services/brand.service';

@NgModule( {
    imports:      [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrandRoutingModule,

        ButtonsModule,
        GridModule,
        DialogModule,
        MaterialModule,

        CoreModule,
    ],
    declarations: [ BrandComponent ],
    providers:    [ BrandService ]
} )
export class BrandModule {
}

export * from './brand.component';
export * from './brand-routing.module';
export * from './services/brand.service';