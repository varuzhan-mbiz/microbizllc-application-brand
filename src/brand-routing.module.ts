import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { BrandComponent } from "./brand.component";
import { AuthGuardService } from "@microbizllc/application-core";

export const routes: Routes = [
    { path: 'brand', component: BrandComponent, canActivate: [ AuthGuardService ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }

