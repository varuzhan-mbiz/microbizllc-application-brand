import { Injectable } from '@angular/core';
import { ApiUrlService,
         HttpService,
         ApiResponse} from "@microbizllc/application-core";


@Injectable()
export class BrandService {

  constructor( private httpService: HttpService, private apiUrlService: ApiUrlService) { }

  getBrand( data?:any ) {

    let url = this.apiUrlService.brand;

    return this.httpService.get( url, data ).map( (resp: ApiResponse) => resp._embedded.brand )

  }

  getDisabledBrands(){

    return this.getBrand( { status: 0 } );

  }

  getEnabledBrands() {

    return this.getBrand( { status: 1 } );

  }

  createBrand( data: any ) {

    let url = this.apiUrlService.brand;

    return this.httpService.post( url, data );

  }

  editBrand( data: any ) {

    let url = `${this.apiUrlService.brand}/${data.brand_id}`;

    delete data.brand_id;

    return this.httpService.patch( url, data );

  }

  removeBrand( data: any ) {

    let url = `${this.apiUrlService.brand}/${data.brand_id}`;

    let removeObj = {status: 0 };

    return this.httpService.patch( url, removeObj );

  }

}
