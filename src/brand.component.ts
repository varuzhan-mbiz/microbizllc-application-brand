import { Observable }  from 'rxjs';
import { Component,
         OnInit }      from '@angular/core';
import { FormGroup,
         FormControl,Validators } from '@angular/forms';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State }          from '@progress/kendo-data-query';

import { ApiResponse } from '@microbizllc/application-core';

import { BrandService } from './services/brand.service';


@Component({
  selector: 'mbiz-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {

  constructor( public brandService: BrandService ) { }

  ngOnInit() {

  }

  brands:any[] = [];

  ngAfterViewInit(){

    this.getBrands();

  }

  newBrand:any = {};

  getBrands(){
    this.brandService.getBrand().subscribe( resp => this.brands = resp );
  }

  opened: boolean = false;
  public close() {
    this.opened = false;
  }

  public open() {
    this.opened = true;
  }


  public view: Observable<GridDataResult>;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public formGroup: FormGroup;

  private editedRowIndex: number;
  private editedRowId: number;

  handleFormSubmit(e){

    e.preventDefault();

    if(this.formGroup) return;

    this.brandService.createBrand( this.newBrand ).subscribe( () => {

      this.close();
      this.newBrand = {};

    } );

  }

  public onStateChange(state: State) {
    this.gridState = state;

    this.getBrands();
  }

  protected editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      'brand_name': new FormControl( dataItem.brand_name, Validators.required ),
      'brand_desc': new FormControl( dataItem.brand_desc ),
    });

    this.editedRowIndex = rowIndex;
    this.editedRowId    = dataItem.brand_id;

    sender.editRow(rowIndex, this.formGroup);
  }

  protected cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.editedRowId = undefined;
    this.formGroup = undefined;
  }

  protected saveHandler({sender, rowIndex, formGroup, isNew}) {

    const brand: any = formGroup.value;

    brand.brand_id = this.editedRowId;

    this.brandService.editBrand( brand ).subscribe( () => sender.closeRow(rowIndex) );

  }

  protected removeHandler({dataItem}) {

    this.brandService.removeBrand( dataItem );

  }

}
